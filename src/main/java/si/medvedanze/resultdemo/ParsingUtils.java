package si.medvedanze.resultdemo;

public class ParsingUtils {
    /**
     * Function to check if provided number is numeric.
     * @param val - String value to be checked
     * @return true/false
     */
    public static boolean isNumeric(String val)
    {
        try
        {
            Integer.parseInt(val);
            return true;
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
    }

    /**
     * Function returns integer value from provided string if possible. Otherwise it returns Integer.MIN_VALUE
     * @param val - String value to be parsed
     * @return Integer value of string or Integer.MIN_VALUE as failure
     */
    public static int getNumericValue(String val)
    {
        if(isNumeric(val))
            return Integer.parseInt(val);
        else
            return Integer.MIN_VALUE;
    }
}

package si.medvedanze.resultdemo.model;

public class WebsiteInfo {

    private String title;
    private Integer httpReturnCode;

    public WebsiteInfo(String content, Integer httpReturnCode) {
        try
        {
            this.title = content.substring(content.indexOf("<title>")+7, content.indexOf("</title>"));
        }
        catch (Exception e)
        {
            this.title = "";
        }

        this.httpReturnCode = httpReturnCode;
    }

    public String getTitle() {
        return title;
    }

    public Integer getHttpReturnCode() {
        return httpReturnCode;
    }

    public void setHttpReturnCode(Integer httpReturnCode) {
        this.httpReturnCode = httpReturnCode;
    }
}

package si.medvedanze.resultdemo.model;

import java.util.ArrayList;

public class WebsiteStatusResponse {

    private long successfulCalls;
    private long failedCalls;
    private ArrayList<WebsiteInfo> fetchedWebsiteInfos;

    public WebsiteStatusResponse(ArrayList<WebsiteInfo> fetchedWebsiteInfos) {
        this.fetchedWebsiteInfos = fetchedWebsiteInfos;
        this.successfulCalls = fetchedWebsiteInfos.stream()
                                                  .filter(websiteInfo -> websiteInfo.getHttpReturnCode() == 200)
                                                  .count();
        this.failedCalls = fetchedWebsiteInfos.size() - this.successfulCalls;
    }

    public long getSuccessfulCalls() {
        return successfulCalls;
    }

    public long getFailedCalls() {
        return failedCalls;
    }

    public ArrayList<WebsiteInfo> getFetchedWebsiteInfos() {
        return fetchedWebsiteInfos;
    }
}

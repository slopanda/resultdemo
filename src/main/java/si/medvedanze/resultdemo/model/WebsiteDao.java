package si.medvedanze.resultdemo.model;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;

@Component
public class WebsiteDao {
    public static final Integer MAX_SESSION_ID = 1_000_000;

    private static Integer sessionId = 0;
    private static HashMap<Integer, ArrayList<WebsiteInfo>> hmSessionWsData = new HashMap<>();
    public static ArrayList<String> websiteUrls = new ArrayList<>();

    static
    {
        websiteUrls.add("https://www.result.si/projekti/");
        websiteUrls.add("https://www.result.si/o-nas/");
        websiteUrls.add("https://www.result.si/kariera/");
        websiteUrls.add("https://www.result.si/blog/");
    }

    /**
     * Get next available session id.
     * @return Available session identificator in range (0, MAX_SESSION_ID)
     */
    public static Integer getSessionId()
    {
        if(sessionId > MAX_SESSION_ID)
        {
            sessionId = 0;
        }
        return sessionId++;
    }

    /**
     * Store obtained website information for given session.
     * @param sessionId unique session identifier
     * @param websiteInfo webiste information
     */
    public static void storeWebsiteDataForSession(Integer sessionId, WebsiteInfo websiteInfo)
    {
        if(!hmSessionWsData.containsKey(sessionId))
        {
            hmSessionWsData.put(sessionId, new ArrayList<>());
        }
        hmSessionWsData.get(sessionId).add(websiteInfo);
    }

    /**
     * Removes website information for provided session id
     * @param sessionId unique session identifier
     */
    public static void clearSessionData(Integer sessionId)
    {
        hmSessionWsData.remove(sessionId);
    }

    /**
     * Fetch website information for provided session.
     * @param sessionId unique session identifier
     * @return arraylist of stored website info
     */
    public ArrayList<WebsiteInfo> fetchSessionData(Integer sessionId)
    {
        return hmSessionWsData.get(sessionId);
    }
}

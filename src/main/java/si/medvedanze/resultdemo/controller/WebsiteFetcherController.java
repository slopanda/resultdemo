package si.medvedanze.resultdemo.controller;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import si.medvedanze.resultdemo.ParsingUtils;
import si.medvedanze.resultdemo.model.WebsiteDao;
import si.medvedanze.resultdemo.model.WebsiteInfo;
import si.medvedanze.resultdemo.model.WebsiteStatusResponse;
import si.medvedanze.resultdemo.service.WebsiteFetcherService;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.stream.IntStream;

@RestController
public class WebsiteFetcherController {

    @Autowired
    private WebsiteDao websiteDao;

    @Autowired
    private WebsiteFetcherService websiteFetcherService;

    /**
     * Based on provided parameter value function creates parallel/sequential calls to list of predefined websites
     * and returns number of successful/failed calls and website titles.
     * @param numCalls number of calls that should be executed in parallel
     * @return WebsiteStatusResponse object
     */
    @RequestMapping(value = "fetchTitles/{numCalls}",
                    method = RequestMethod.GET,
                    produces={MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Fetch titles of provided websites",
            notes = "Returns number of successful/failed calls and website titles",
            response = WebsiteStatusResponse.class)
    @ApiResponses(value = { @ApiResponse(code = 400, message = "Provided parameter is not a number"),
                            @ApiResponse(code = 200, message = "OK") })
    public @ResponseBody ResponseEntity fetchWebsiteTitles(@ApiParam(value = "Number of parallel calls", required = true)
                                                           @PathVariable("numCalls") String numCalls)
    {
        // Handle error
        if(!ParsingUtils.isNumeric(numCalls)){
            ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
            return new ResponseEntity<String>("Provided parameter is not a number", HttpStatus.BAD_REQUEST);
        }

        int iNumCalls = ParsingUtils.getNumericValue(numCalls);

        // Get session
        Integer sessionId = websiteDao.getSessionId();

        // handle async
        CompletableFuture.allOf(IntStream.range(0, iNumCalls)
                                         .mapToObj(idx -> websiteFetcherService.fetchWebsiteAsync(websiteDao.websiteUrls.get(idx), sessionId))
                                         .toArray(CompletableFuture<?>[]::new))
                         .join();

        // handle sequential
        for(int i = iNumCalls; i < websiteDao.websiteUrls.size(); i++)
        {
            websiteFetcherService.fetchWebsite(websiteDao.websiteUrls.get(i), sessionId);
        }

        // gather session data and clear session storage
        ArrayList<WebsiteInfo> fetchedInfos = new ArrayList<>(websiteDao.fetchSessionData(sessionId));
        websiteDao.clearSessionData(sessionId);

        // create response entity
        return new ResponseEntity<>(new WebsiteStatusResponse(fetchedInfos), HttpStatus.OK);
    }
}

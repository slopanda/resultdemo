package si.medvedanze.resultdemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import si.medvedanze.resultdemo.model.WebsiteDao;
import si.medvedanze.resultdemo.model.WebsiteInfo;

import java.util.concurrent.CompletableFuture;

@Service
public class WebsiteFetcherService {

    @Autowired
    private WebsiteDao websiteDao;

    /**
     * Function fetches website title and http response status code value (supports ASYNC)
     * @param url - website URL
     * @param sessionId - current session ID
     */
    @Async
    public CompletableFuture<String> fetchWebsiteAsync(String url, Integer sessionId)
    {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> websiteResponse = restTemplate.getForEntity(url, String.class);
        WebsiteInfo wi = new WebsiteInfo(websiteResponse.getBody(), websiteResponse.getStatusCodeValue());
        websiteDao.storeWebsiteDataForSession(sessionId, wi);

        System.out.println(Thread.currentThread().getName()+ " > Stored : "+wi.getTitle());

        return CompletableFuture.completedFuture(wi.getHttpReturnCode().toString());
    }

    /**
     * Function fetches website title and http response status code value
     * @param url - website URL
     * @param sessionId - current session ID
     */
    public void fetchWebsite(String url, Integer sessionId)
    {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> websiteResponse = restTemplate.getForEntity(url, String.class);
        WebsiteInfo wi = new WebsiteInfo(websiteResponse.getBody(), websiteResponse.getStatusCodeValue());
        websiteDao.storeWebsiteDataForSession(sessionId, wi);

        System.out.println(Thread.currentThread().getName()+ " > Stored : "+wi.getTitle());
    }
}

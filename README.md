### Building the application
```
gradle bootJar
```

### Running the application
```
java -jar build/libs/resultdemo-1.0-RELEASE.jar
```

### Dockerhub image
Solution is also available as Docker container. Execute following commands to fetch image with solution and run it locally.
```
docker pull anzzemedved/resultdemo:latest
docker run -p 8080:8080 -t anzzemedved/resultdemo
```
Endpoint is available at [localhost:8080](localhost:8080).

### Sample usage
* Swagger documentation: [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)
* Endpoint usage (GET request) : [http://localhost:8080/fetchTitles/1](http://localhost:8080/fetchTitles/1)